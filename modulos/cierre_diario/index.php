<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;

class SubIndex
{
    public function execute($arr)
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $movType = $arr['params'][0];
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=> "Cierre Diario"]);
        $result = \myApp2\CierreDiario::get('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('CIERRE', Func::change_date_format($row['cierre']));
            $xtpl->assign('CATEGORIA', $row['categoria']);
            $xtpl->assign('PRODUCTO', $row['producto']);
            $xtpl->assign('MARCA', $row['marca']);
            $xtpl->assign('EMPAQUE_DES', $row['empaque_des']);  
            $xtpl->assign('ACUMULADO', $row['acumulado']);        
            $xtpl->assign('ENTRADAS', $row['entradas']);
            $xtpl->assign('SALIDAS', $row['salidas']);
            $xtpl->assign('REVERSO_ENTRADAS', $row['reverso_entradas']);
            $xtpl->assign('REVERSO_SALIDAS', $row['reverso_salidas']);
            $xtpl->assign('TOTAL', $row['total']);
            Func::btnRecordSearch( $xtpl ,["btnId"=>$row['id_presentacion']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[
            ["btnName"=>"Exit"],["btnName"=>"Print" , "btnClick"=>"submitPrint();"]
        ]);


            

        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
