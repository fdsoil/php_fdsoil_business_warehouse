function submitInsert(val)
{
    relocate('../cierre_diario_aux/', {});
}

function submitEdit(val)
{
    relocate('../cierre_diario_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}

function submitPrint()
{
    let desde = document.getElementById('id_cierre_dia_desde').value;
    let hasta = document.getElementById('id_cierre_dia_hasta').value;
    relocateBlank('../../../'+aMyApp[2]+'/reportes/cierre_diario/pdf/', {date_from: desde,  date_until: hasta} );
}

