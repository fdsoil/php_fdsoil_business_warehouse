<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;
use \FDSoil\XTemplate;
use \myApp2\CierreDiario;

class SubIndex
{
    public function execute($aReqs)
    {
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        //$aRegist = array_key_exists('id', $_POST)
        //    ? DbFunc::fetchAssoc(Marca::get('REGIST'))
        //        :\FDSoil\DbFunc::iniRegist('marca','public', 'producto');
       $arr=CierreDiario::getCierreFecPending();
      //Func::seeArray($arr);
        $xtpl->assign('CIERRE_DIA_DESDE', Func::change_date_format($arr['desde']));
        $xtpl->assign('CIERRE_DIA_HASTA', Func::change_date_format($arr['hasta']));
        //$xtpl->assign('ID', $aRegist['id']);
        //$xtpl->assign('NOMBRE', $aRegist['nombre']);
        //$xtpl->assign('NOMBRE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        //$xtpl->assign('PRODUCTOR_ID', $aRegist['productor_id']);
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "cierre_diario"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
