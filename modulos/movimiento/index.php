<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;

class SubIndex
{
    public function execute($arr)
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $movType = $arr['params'][0];
        $aView['load'] = '['.$_SESSION['menu'].',"'.$movType.'"]';
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aMovType = \myApp2\Movimiento::movTypeConfig($movType);
        $xtpl->assign('MOV_TYPE_LABEL', $aMovType['label']);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=> $aMovType['label']]);
        $result = \myApp2\Movimiento::get(strtoupper($movType), 'notClosed');
        while ($row = DbFunc::fetchAssoc($result)){
            //$xtpl->assign('ID_MOVIMIENTO_TIPO', $row['id_movimiento_tipo']);
            $xtpl->assign('MOVIMIENTO_NUM', $row['movimiento_num']);
            $xtpl->assign('MOVIMIENTO_FEC', Func::change_date_format($row['movimiento_fec']));
            $xtpl->assign('SUJETO', $row['sujeto']);
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            //$xtpl->assign('ID_SOPORTE_TIPO', $row['id_soporte_tipo']);
            $xtpl->assign('SOPORTE_NUM', $row['soporte_num']);
            //$xtpl->assign('SOPORTE_FEC', Func::change_date_format($row['soporte_fec']));
            //$xtpl->assign('OBSERVACION', $row['observacion']);
            //$xtpl->assign('CIERRE_FEC', Func::change_date_format($row['cierre_fec']));            
            $xtpl->assign('DES_MOVIMIENTO_TIPO', $row['des_movimiento_tipo']);
            $xtpl->assign('DES_SOPORTE_TIPO', $row['des_soporte_tipo']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

