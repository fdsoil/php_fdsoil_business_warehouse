function submitInsert(val)
{
    relocate('../../movimiento_aux/' + movType + '/', {});
}

function submitEdit(val)
{
    relocate('../../movimiento_aux/' + movType + '/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/' + movType + '/', {'id':val});
        });
}

