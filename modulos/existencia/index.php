<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;

class SubIndex
{
    public function execute($arr)
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $movType = $arr['params'][0];
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $arr = \myApp2\Existencia::get('LIST');
        //Func::seeArray($arr);
        foreach ($arr as $clave => $valor) {
            $xtpl->assign('CATEGORIA', $valor['categoria']);
            $xtpl->assign('PRODUCTO', $valor['producto']);
            $xtpl->assign('MARCA', $valor['marca']);
            $xtpl->assign('PRESENTACION', $valor['presentacion']);          
            $xtpl->assign('ENTRADAS', $valor['entradas']);
            $xtpl->assign('SALIDAS', $valor['salidas']);
            $xtpl->assign('REVERSO_ENTRADAS', $valor['reverso_entradas']);
            $xtpl->assign('REVERSO_SALIDAS', $valor['reverso_salidas']);
            $xtpl->assign('TOTAL', $valor['total']);
            Func::btnRecordSearch( $xtpl ,["btnId"=>$valor['id_presentacion']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}
