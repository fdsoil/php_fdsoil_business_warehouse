<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;
//use \myApp1\Categoria;
use \myApp2\Movimiento\MovimientoAux;

trait Solapa1
{
    private function _solapa1()
    {
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa1.html");
        Func::appShowId($xtpl);
        /*$result = Categoria::get('WITH_ASSOC_PRESENTATION');
        while ($row = DbFunc::fetchAssoc($result)) {
            $xtpl->assign('ID_CATEGORIA', $row['id']);
            $xtpl->assign('DES_CATEGORIA', $row['familia']);
            $xtpl->parse('main.categoria');
        }*/

        $xtpl->assign('IS_REVERSE_THEN_DISPLAY_NONE', in_array($this->_aMovType['id'], [3,4]) ? 'none' : '');



        if (array_key_exists('id', $_POST)) {
            $matrix = MovimientoAux::get();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('ID_PRESENTACION', $arr['id_presentacion']);
                $xtpl->assign('DES_PRESENTACION', 
                    $arr['producto'] 
                    . ' ( ' . $arr['marca'] . ' ) | '
                    . $arr['empaque_des'] 
                );
                $empaques=json_decode($arr['empaque_json']);
                $empaqueUltimo = end($empaques);
                $unidad = array_shift($empaques); 
                $nTotUnidad=1;
                foreach ($empaques as $obj)
                    $nTotUnidad*=$obj->quantity;
                $nTotUnidad *= $arr['cantidad'];
                $xtpl->assign('CANTIDAD_X_PRESENTACION', $arr['cantidad'] . ' ' . $empaqueUltimo->packing );
                $xtpl->assign('CANTIDAD_X_UNIDAD', $nTotUnidad .' '. $unidad->packing );
                $xtpl->parse('main.tab_movimiento_aux');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

