<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;
use \myApp2\Movimiento;

trait Solapa0
{
    private function _solapa0()
    {
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa0.html");
        Func::appShowId($xtpl);
        $xtpl->assign('IS_REVERSE_THEN_READONLY', in_array($this->_aMovType['id'], [3,4]) ? 'readonly' : '');
        $xtpl->assign('MOV_TYPE_LABEL', $this->_aMovType['label']);
        $xtpl->assign('MOVIMIENTO_NUM', $this->_aRegist['movimiento_num']);
        $xtpl->assign('MOVIMIENTO_NUM_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('MOVIMIENTO_FEC', Func::change_date_format($aRegist['movimiento_fec']));
        $xtpl->assign('SUJETO', $this->_aRegist['sujeto']);
        $xtpl->assign('DESCRIPCION', $this->_aRegist['descripcion']);
        $xtpl->assign('SOPORTE_NUM', $this->_aRegist['soporte_num']);
        $xtpl->assign('SOPORTE_FEC', Func::change_date_format($this->_aRegist['soporte_fec']));
        $xtpl->assign('SUJETO', $this->_aRegist['sujeto']);

        if ($this->_aMovType['id']== 3 || $this->_aMovType['id'] == 4) {
            $var=strtoupper(explode("_", $this->_movType)[1]);
            $xtpl->assign('PICKOPEN', 
                "onclick=\"pickOpen(
                    'soporte_num',
                    'id_soporte_num',
                    '../../../".$_SESSION['aMyApp'][2]."/pickList/control/movimiento/$var',
                    75, 75, 200, 50, 'si');\""
            );
        }

        //$xtpl->assign('OBSERVACION', $this->_aRegist['observacion']);
        $xtpl->assign('OBSERVACION', 'S/O');
        $xtpl->assign('CIERRE_FEC_CHK', $this->_aRegist['cierre_fec'] === 't' ? 'checked' : '');
        $result = Movimiento::movimientoTipoList();
        while ($row = DbFunc::fetchRow($result)) {
            if ($this->_aMovType['id'] == $row[0]) {            
                $xtpl->assign('ID_MOVIMIENTO_TIPO', $row[0]);
                $xtpl->assign('DES_MOVIMIENTO_TIPO', $row[1]);
                $xtpl->assign('SELECTED_MOVIMIENTO_TIPO', ($this->_aRegist['id_movimiento_tipo'] == $row[0]) ? 'selected' : '');
                $xtpl->parse('main.movimiento_tipo');
            }
        }
        $result = Movimiento::soporteTipoList();
        while ($row = DbFunc::fetchRow($result)) {
            if ($this->_aMovType['id'] == $row[2]) {                          
                $xtpl->assign('ID_SOPORTE_TIPO', $row[0]);
                $xtpl->assign('DES_SOPORTE_TIPO', $row[1]);
                $xtpl->assign('SELECTED_SOPORTE_TIPO', ($this->_aRegist['id_soporte_tipo'] == $row[0]) ? 'selected' : '');
                $xtpl->parse('main.soporte_tipo');
            }  
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}






