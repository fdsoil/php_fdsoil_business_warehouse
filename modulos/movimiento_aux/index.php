<?php
use \FDSoil\DbFunc;
use \FDSoil\Func;

require_once(__DIR__."/Solapa0.php");
require_once(__DIR__."/Solapa1.php");

class SubIndex
{
    use Solapa0, Solapa1;

    private $_aRegist = [];
    private $_aView = [];
    private $_movType = '';
    private $_aMovType = [];

    public function __construct()
    {
        \FDSoil\Audit::validaReferenc();
        $this->_aView["include"] = Func::getFileJSON(__DIR__."/js/include.json");
        $this->_aView["userData"] = Func::usuarioData();
        $this->_aView["load"] = array_key_exists("id", $_POST)?$_POST["id"]:0;
        $this->_aRegist = \myApp2\Movimiento::loadRegist();
    }

    public function execute($arr)
    {
        $this->_movType = $arr['params'][0];
        $this->_aMovType = \myApp2\Movimiento::movTypeConfig($this->_movType);
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $xtpl->assign('MOV_TYPE_LABEL', $this->_aMovType['label']);
	Func::numberDate(
            $xtpl, [
                $this->_aRegist['movimiento_num'],
                Func::change_date_format($this->_aRegist['movimiento_fec'])
            ]);
        $xtpl->assign("ID", $this->_aRegist["id"]);
        if (!array_key_exists("id", $_POST)) {
            $xtpl->assign("TAB_NONE_BLOCK1", "none");
            $xtpl->assign("BOTONES_NONE_BLOCK", "none");
        }
        $aSolapa[0] = self::_solapa0();
        $aSolapa[1] = self::_solapa1();
        Func::bldSolapas($xtpl, $aSolapa);
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "../movimiento/".$this->_movType],
                                            ["btnName" => "Save", "btnClick"=> "Movimiento.valEnvio('$this->_movType');"],
                                            ["btnName" => "Close", "btnClick"=> "valEnvio(2);", "btnLabel" => "Movimiento", "btnDisplay" => "none"]]);
        $xtpl->parse("main");
        $this->_aView["content"] = $xtpl->out_var("main");
        return $this->_aView;
    }
}

