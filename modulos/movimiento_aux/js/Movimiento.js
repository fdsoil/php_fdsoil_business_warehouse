var Movimiento = (function () {

    let rqsRegister = function(sStatus) {
        let responseAjax = (response) => {
            msjAdmin(response[0]);
            if (inArray(response[0], ['C', 'A', 'H'])) {
                if (response[0] == 'C') {
                    document.getElementById('id').value = response[1];
                    document.getElementById('id_form_number').textContent = response[2];
                    document.getElementById('id_form_date').textContent = changeDateFormat(response[3], "/");
                    show('Tab1', 'explode', 1500);
                    eventListenerActivar();
                } else if (response[0] == 'H') {
                    document.getElementById('popup_ok').setAttribute('onClick', 'window.location.assign("../movimiento/")');
                }
            }
        }
        let sIdVal = document.getElementById('id').value;
        let oPanel = document.getElementById('div0');
        let reqs = b64EncodeUnicode('id') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);// + '&id_status=' + sStatus;
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = PATH_MYAPP + "reqs/control/movimiento/index.php/register";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.dataType = 'json';
        ajax.b64 = true;
        ajax.send();
    }

    let edit = function(response){
        document.getElementById('id_movimiento_tipo').value = 3;
        document.getElementById('id_soporte_tipo').value = 5;//response.id_movimiento_tipo;
        document.getElementById('id_soporte_fec').value = changeDateFormat(response.movimiento_fec, "/"); 
        document.getElementById('sujeto').value = '';
        document.getElementById('descripcion').value = '';
        document.getElementById('observacion').innerHTML = 'Tipo de Soporte: '+ response.des_soporte_tipo 
                                                         + '; N° de Soporte: ' + response.soporte_num 
                                                         + '; Sujeto: ' + response.sujeto 
                                                         + '; Descripción: ' + response.descripcion + ';';
    }

    let sweepBoard = function() {
        let aRows = document.getElementById('id_tab_movimiento_aux').getElementsByTagName('tbody')[0].rows;
        let oArr = [], idPresentacion, cantidad;
        for (let i=0; i < aRows.length; i++) {
            idPresentacion = aRows[i].cells[0].id;
            cantidad = aRows[i].cells[1].textContent.split(" ")[0];
            oArr[i] = `'{"id": 0, "id_presentacion": ${idPresentacion} , "cantidad": ${cantidad}}'::json`;
        }
        return JSON.stringify(oArr);
    }

    let prepareSubmit = function(movType){
        document.getElementById('str_arr_table').value = sweepBoard();
        relocate('../save/' , {
            'id'                : document.getElementById('id').value,
            'id_movimiento_tipo': document.getElementById('id_movimiento_tipo').value,
            'id_soporte_tipo'   : document.getElementById('id_soporte_tipo').value,
            'soporte_num'       : document.getElementById('soporte_num').value,
            'soporte_fec'       : document.getElementById('id_soporte_fec').value,
            'sujeto'            : document.getElementById('sujeto').value,
            'descripcion'       : document.getElementById('descripcion').value,
            'observacion'       : document.getElementById('observacion').value,
            'str_arr_table'     : document.getElementById('str_arr_table').value
        });        
    }

    return {
        valEnvio: function(movType) {

            if (validateObjs(document.getElementById('div0'))){

                if (inArray(movType, ['reverso_entrada','reverso_salida'])) {
                    prepareSubmit(movType);//submit()
                } else if (inArray(movType, ['entrada','salida'])) {
                    rqsRegister();
                }
            }
        },
        reqGet: function(id) {
            let responseAjax = (response) => {
                edit(response[0]);
            }
            let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
            let ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = PATH_MYAPP + "reqs/control/movimiento/index.php/get";
            ajax.data = reqs;
            ajax.funResponse = responseAjax;
            ajax.dataType = 'json';
            ajax.b64 = true;
            ajax.send();
        }
    };

})();


