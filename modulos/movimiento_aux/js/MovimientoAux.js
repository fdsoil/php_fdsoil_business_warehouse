var MovimientoAux = (function () {

    const MYPATH = PATH_MYAPP + "reqs/control/movimiento/movimiento_aux.php/";

    let initObjsPanel = function() {
        initObjs(document.getElementById('id_panel_movimiento_aux'));
    }

    let rqsRegister = function() {
        let sIdVal = document.getElementById('id').value;
        let responseAjax = (response) => {
            msjAdmin(response);
            if (inArray(response, ['C', 'A'])){
                MovimientoAux.reqGet(sIdVal);
                MovimientoAux.onOffPanel();
            }
        }
        let oPanel = document.getElementById('id_panel_movimiento_aux');
        let reqs = b64EncodeUnicode('id_movimiento') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
        let ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = MYPATH + "register";
        ajax.data = reqs;
        ajax.funResponse = responseAjax;
        ajax.b64 = true;
        ajax.send();
    }    

    let fillTab = function (aObjs) {

        let fillTabAux = function(row) {

            let oTr = document.createElement('tr');
            oTr.id = row.id;
            let aTd = [];

            aTd[0] = document.createElement('td');
            aTd[0].id = row.id_presentacion;
            aTd[0].appendChild(document.createTextNode(row.producto + ' ( ' + row.marca +' ) | '+row.empaque_des));
            oTr.appendChild(aTd[0]);

            let empaques = JSON.parse(row.empaque_json);
            let empaqueUltimo = empaques[empaques.length-1];

            let unidad =  empaques.shift(); 
            let nTotUnidad=1;

            for ( let i = 0; i< empaques.length; i++){
                nTotUnidad *= empaques[i].quantity; 
            }            
            nTotUnidad *= row.cantidad;       

            aTd[1] = document.createElement('td');
            aTd[1].appendChild(document.createTextNode(row.cantidad + ' '+ empaqueUltimo.packing));
            oTr.appendChild(aTd[1]);

            aTd[2] = document.createElement('td');
            aTd[2].appendChild(document.createTextNode(nTotUnidad + ' ' + unidad.packing));
            oTr.appendChild(aTd[2]);

            if (inArray(document.getElementById('id_movimiento_tipo').value,[1,2])) {

            let oImgEdit = document.createElement('img');
            oImgEdit.setAttribute('class', 'accion');
            oImgEdit.setAttribute('src', PATH_APPORG +'img/edit.png');
            oImgEdit.setAttribute('title', 'Editar datos...');
            oImgEdit.setAttribute('onclick',"MovimientoAux.edit(this);valBtnClose();");
            aTd[9] = document.createElement('td');
            aTd[9].align = 'center';
            aTd[9].appendChild(oImgEdit);

            let oImgDelete = document.createElement('img');
            oImgDelete.setAttribute('class', 'accion');
            oImgDelete.setAttribute('src', PATH_APPORG + 'img/cross.png');
            oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
            oImgDelete.setAttribute('onclick',"MovimientoAux.rqsRemove("+row.id+");valBtnClose();");
            aTd[9].appendChild(oImgDelete);
            oTr.appendChild(aTd[9]);
            }

            return oTr;

        }

        let tbl = document.getElementById('id_tab_movimiento_aux');
        Table.deleteAllRows('id_tab_movimiento_aux',1);

        if (!tbl.tBodies[0]) {
            var tbody = document.createElement('tbody');
            tbody.setAttribute('style', 'display: block');
            tbody.setAttribute('style', 'width: 100%');
            tbl.appendChild(tbody);
        }

        if (aObjs.length !== 0) {
            for (var i = 0; i < aObjs.length; i++)
                tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
            Table.paintTRsClearDark('id_tab_movimiento_aux');
        }

    }
    let _valEnvio = function () {
        let resp = false;
        let movType = document.getElementById('id_movimiento_tipo').value;
        let cantidad = document.getElementById('cantidad').value;
        let existencia = document.getElementById('div_existencia').textContent;
        let compareExistence = function (a,b) { return a*1 <= b*1; }
        if (validateObjs(document.getElementById("id_panel_movimiento_aux")))
             resp = (movType == 2) ? compareExistence(cantidad,existencia) : true;       
        return resp;
    }


    return {

        onOffPanel: function () {
            toggle("id_panel_movimiento_aux", "blind",500);
            changeTheObDeniedWritingImg("id_img_movimiento_aux");
            initObjsPanel();
            document.getElementById("id_movimiento_aux").value=0;
        },

        valEnvio: function () {
            _valEnvio() ? rqsRegister() : alert('Disculpe: La cantidad supera la existencia.');            
        },

        edit: function (obj) {
            if(document.getElementById('id_panel_movimiento_aux').style.display == "none")
                MovimientoAux.onOffPanel();
            document.getElementById("id_movimiento_aux").value = obj.parentNode.parentNode.id;
            let oPanel = document.getElementById("id_panel_movimiento_aux");
            let oInputs = oPanel.getElementsByTagName("input");
            oInputs['id_presentacion'].value = obj.parentNode.parentNode.cells[0].id;
            //oInputs['presentacion'].value = obj.parentNode.parentNode.cells[0].textContent;
            oInputs['cantidad'].value = obj.parentNode.parentNode.cells[1].textContent.split(" ")[0];
            let oTextAreas = oPanel.getElementsByTagName("textarea");
            oTextAreas['presentacion'].value = obj.parentNode.parentNode.cells[0].textContent;
        },

        reqGet: function (id) {
            let responseAjax = (response) => {
                fillTab(response);
                valBtnClose();
            }
            let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
            let ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = MYPATH + "get";
            ajax.data = reqs;
            ajax.funResponse = responseAjax;
            ajax.dataType = 'json';
            ajax.async = true;
            ajax.b64 = true;
            ajax.send();
        },

        rqsRemove: function (id) {
            confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
                if (ok) {
                    let responseAjax = (response) => {
                        (response != 'B') ? msjAdmin(response) : MovimientoAux.reqGet();
                    }
                    let ajax = new sendAjax();
                    ajax.method = 'POST';
                    ajax.url = MYPATH + "remove";
                    ajax.data = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(id);
                    ajax.funResponse = responseAjax;
                    ajax.b64 = true;
                    ajax.send();
                }
            });
        }

    };

})();

