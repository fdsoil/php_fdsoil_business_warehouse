var Existencia = (function () {

    const MYPATH = PATH_MYAPP + "reqs/control/existencia/existencia.php/";

    return {

        reqGet: function (val) {
            let responseAjax = (response) => {
                document.getElementById('div_existencia').textContent=response.length>0 ? response[0].total : 0;
            }
            //let reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(val);
            let reqs = "id=" + val;
            let ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = MYPATH + "get";
            ajax.data = reqs;
            ajax.funResponse = responseAjax;
            ajax.dataType = 'json';
            //ajax.async = bAsync;
            //ajax.b64 = true;
            ajax.send();
        }

    };

})();

