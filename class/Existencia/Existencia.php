<?php
namespace myApp2;

use \FDSoil\Func;
use \FDSoil\DbFunc;

class Existencia
{

    const PATH = __DIR__ . '/sql/existencia/';

    static private $existencias = [];

    function get($label)
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE id_presentacion = {fld:id} ');
                break;

        }
        self::$existencias = DbFunc::fetchAll(DbFunc::exeQryFile(self::PATH . "get.sql", $arr, false, '', 'almacen'));
        self::getDespliegue();
        return self::$existencias;
    }

    private function getDespliegue() {

        for ($i=0; $i<count(self::$existencias); $i++)
            self::$existencias[$i]['presentacion']=self::findDespliegue(self::$existencias[$i]);
    }

    private function findDespliegue($arr) {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "despliegue_get.sql", $arr, false, '', 'almacen'));
        return $row[0];
    }

/*

*/

}

