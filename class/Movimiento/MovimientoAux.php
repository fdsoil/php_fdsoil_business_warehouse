<?php
namespace myApp2\Movimiento;

use \FDSoil\Func;
use \FDSoil\DbFunc;

/** MovimientoAux: Clase para actualizar y consultar la tabla 'movimiento_aux'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class MovimientoAux
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase MovimientoAux.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase MovimientoAux.*/
    const PATH = __DIR__ . '/sql/movimiento_aux/';

    /** Actualiza registro de la tabla 'movimiento_aux'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'movimiento_aux'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        $_POST['id_usuario'] = $_SESSION['id_usuario'];
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO', 'almacen'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'movimiento_aux'.
    * Descripción: Obtener registro(s) de la tabla 'movimiento_aux'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'movimiento_aux'.*/
    public function get()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::PATH . "get.sql", $_POST, false, '', 'almacen'));
    }

    /** Elimina registro de la tabla 'movimiento_aux'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'movimiento_aux'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO', 'almacen'));
        return $row[0];
    }

    /** Lista de tabla foránea 'categoria'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'categoria'.
    * @return result Resultado de la consulta de la de tabla foránea 'categoria'.*/ 
    //public function categoriaList()
    //{
    //    return DbFunc::exeQryFile(self::PATH . 'categoria_list_select.sql', $_POST, false, '', 'negocio_producto');
    //}

}

