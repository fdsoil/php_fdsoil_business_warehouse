<?php
namespace myApp2\Movimiento;

class WhereSalida
{

    static public function all()
    {
        return ' A.id_movimiento_tipo = 2 ';
    }

    static private function qryStart( $value = true )
    {
        return $value ? self::all() : '';
    }

    static public function notReversed($value = true)
    {
        $str = ' AND A.movimiento_num NOT IN (SELECT soporte_num FROM public.movimiento WHERE id_movimiento_tipo = 4) ';
        return self::qryStart($value) . $str;
    }

    static public function reversed($value = true)
    {
        $str = ' AND A.movimiento_num IN (SELECT soporte_num FROM public.movimiento WHERE id_movimiento_tipo = 4) ';
        return self::qryStart($value) . $str;
    }

    static public function notClosed($value = true)
    {
        return self::qryStart($value) . ' AND cierre_fec IS NULL ';
    }

    static public function closed($value = true)
    {
        return self::qryStart($value) . ' AND cierre_fec IS NOT NULL ';
    }

    static public function notRevesedAndNotClosed($value = true)
    {
        return self::qryStart($value) . self::notReversed(false) . self::notClosed(false);
    }

    static public function notRevesedAndClosed($value = true)
    {
        return self::qryStart($value) . self::notReversed(false) . self::closed(false);
    }

    static public function revesedAndNotClosed($value = true)
    {
        return self::qryStart($value) . self::reversed(false) . self::notClosed(false);
    }

    static public function revesedAndClosed($value = true)
    {
        return self::qryStart($value) . self::reversed(false) . self::closed(false);
    }    

}

