-- Function: public.movimiento_register(integer, integer, character varying, date, character varying, text, integer, character varying, date, text, date, integer, integer, integer, integer, date, date, time without time zone, time without time zone)

-- DROP FUNCTION public.movimiento_register(integer, integer, character varying, date, character varying, text, integer, character varying, date, text, date, integer, integer, integer, integer, date, date, time without time zone, time without time zone);

CREATE OR REPLACE FUNCTION public.movimiento_register(i_id integer, i_id_movimiento_tipo integer, i_movimiento_num character varying, i_movimiento_fec date, i_sujeto character varying, i_descripcion text, i_id_soporte_tipo integer, i_soporte_num character varying, i_soporte_fec date, i_observacion text, i_cierre_fec date, i_id_user_insert integer, i_id_user_update integer, i_id_user_edit integer, i_editing integer, i_date_insert date, i_date_update date, i_time_insert time without time zone, i_time_update time without time zone)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM public.movimiento
                        WHERE movimiento_num=i_movimiento_num;
                IF  v_existe='f' THEN
                        INSERT INTO public.movimiento(
                                id_movimiento_tipo,
                                movimiento_num,
                                movimiento_fec,
                                sujeto,
                                descripcion,
                                id_soporte_tipo,
                                soporte_num,
                                soporte_fec,
                                observacion,
                                cierre_fec,
                                id_user_insert,
                                id_user_update,
                                id_user_edit,
                                editing,
                                date_insert,
                                date_update,
                                time_insert,
                                time_update)
                        VALUES (
                                i_id_movimiento_tipo,
                                i_movimiento_num,
                                i_movimiento_fec,
                                i_sujeto,
                                i_descripcion,
                                i_id_soporte_tipo,
                                i_soporte_num,
                                i_soporte_fec,
                                i_observacion,
                                i_cierre_fec,
                                i_id_user_insert,
                                i_id_user_update,
                                i_id_user_edit,
                                i_editing,
                                i_date_insert,
                                i_date_update,
                                i_time_insert,
                                i_time_update);
                        SELECT max(id) INTO v_id FROM public.movimiento
                                WHERE movimiento_num=i_movimiento_num;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE public.movimiento SET
                        id_movimiento_tipo=i_id_movimiento_tipo,
                        movimiento_fec=i_movimiento_fec,
                        sujeto=i_sujeto,
                        descripcion=i_descripcion,
                        id_soporte_tipo=i_id_soporte_tipo,
                        soporte_num=i_soporte_num,
                        soporte_fec=i_soporte_fec,
                        observacion=i_observacion,
                        cierre_fec=i_cierre_fec,
                        id_user_insert=i_id_user_insert,
                        id_user_update=i_id_user_update,
                        id_user_edit=i_id_user_edit,
                        editing=i_editing,
                        date_insert=i_date_insert,
                        date_update=i_date_update,
                        time_insert=i_time_insert,
                        time_update=i_time_update
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.movimiento_register(integer, integer, character varying, date, character varying, text, integer, character varying, date, text, date, integer, integer, integer, integer, date, date, time without time zone, time without time zone)
  OWNER TO postgres;

-- Function: public.movimiento_remove(integer)

-- DROP FUNCTION public.movimiento_remove(integer);

CREATE OR REPLACE FUNCTION public.movimiento_remove(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.movimiento WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.movimiento_remove(integer)
  OWNER TO postgres;

-- Function: public.movimiento_aux_register(integer, integer, integer, integer, date, integer, integer, date, date, time without time zone, time without time zone)

-- DROP FUNCTION public.movimiento_aux_register(integer, integer, integer, integer, date, integer, integer, date, date, time without time zone, time without time zone);

CREATE OR REPLACE FUNCTION public.movimiento_aux_register(i_id integer, i_id_movimiento integer, i_id_presentacion integer, i_cantidad integer, i_cierre_fec date, i_id_user_insert integer, i_id_user_update integer, i_date_insert date, i_date_update date, i_time_insert time without time zone, i_time_update time without time zone)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO public.movimiento_aux(
                        id_movimiento,
                        id_presentacion,
                        cantidad,
                        cierre_fec,
                        id_user_insert,
                        id_user_update,
                        date_insert,
                        date_update,
                        time_insert,
                        time_update)
                VALUES (
                        i_id_movimiento,
                        i_id_presentacion,
                        i_cantidad,
                        i_cierre_fec,
                        i_id_user_insert,
                        i_id_user_update,
                        i_date_insert,
                        i_date_update,
                        i_time_insert,
                        i_time_update);
                o_return:= 'C';
        ELSE
                UPDATE public.movimiento_aux SET
                        id_movimiento=i_id_movimiento,
                        id_presentacion=i_id_presentacion,
                        cantidad=i_cantidad,
                        cierre_fec=i_cierre_fec,
                        id_user_insert=i_id_user_insert,
                        id_user_update=i_id_user_update,
                        date_insert=i_date_insert,
                        date_update=i_date_update,
                        time_insert=i_time_insert,
                        time_update=i_time_update
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.movimiento_aux_register(integer, integer, integer, integer, date, integer, integer, date, date, time without time zone, time without time zone)
  OWNER TO postgres;

-- Function: public.movimiento_aux_remove(integer)

-- DROP FUNCTION public.movimiento_aux_remove(integer);

CREATE OR REPLACE FUNCTION public.movimiento_aux_remove(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM public.movimiento_aux WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.movimiento_aux_remove(integer)
  OWNER TO postgres;

