SELECT 
    A.id,
    A.id_movimiento_tipo,
    A.movimiento_num,
    A.movimiento_fec,
    A.sujeto,
    A.descripcion,
    A.id_soporte_tipo,
    A.soporte_num,
    A.soporte_fec,
    A.observacion,
    A.cierre_fec,
    A.id_user_insert,
    A.id_user_update,
    A.id_user_edit,
    A.editing,
    A.date_insert,
    A.date_update,
    A.time_insert,
    A.time_update
    ,B.nombre AS des_movimiento_tipo
    ,C.nombre AS des_soporte_tipo
FROM public.movimiento A
INNER JOIN public.movimiento_tipo B ON A.id_movimiento_tipo=B.id
INNER JOIN public.soporte_tipo C ON A.id_soporte_tipo=C.id
{fld:where}
ORDER BY 3;
