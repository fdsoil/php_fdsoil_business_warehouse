SELECT 
    A.id,
    A.id_movimiento,
    A.id_presentacion,
    A.cantidad,
    A.cierre_fec,
    B.categoria,
    B.producto,
    B.marca,
    (SELECT public.view_presentacion_despliegue(A.id_presentacion)) as empaque_des,
    B.empaque_json,
    B.medida_unidad,
    B.precio_costo,
    B.precio_venta,
    B.bar_cod,
    B.int_cod
FROM public.movimiento_aux A
INNER JOIN public.view_presentacion B ON A.id_presentacion = B.id
WHERE id_movimiento = {fld:id}
ORDER BY 3;
