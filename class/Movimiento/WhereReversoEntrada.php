<?php
namespace myApp2\Movimiento;

class WhereReversoEntrada
{

    static public function all()
    {
        return ' A.id_movimiento_tipo = 3 ';
    }

    static private function qryStart( $value = true )
    {
        return $value ? self::all() : '';
    }

    static public function notClosed($value = true)
    {
        return self::qryStart($value) . ' AND cierre_fec IS NULL ';
    }

    static public function closed($value = true)
    {
        return self::qryStart($value) . ' AND cierre_fec IS NOT NULL ';
    }    

}

