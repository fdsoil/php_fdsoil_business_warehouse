<?php
namespace myApp2;

use \FDSoil\Func;
use \FDSoil\DbFunc;
use \myApp2\Movimiento\WhereEntrada;
use \myApp2\Movimiento\WhereSalida;
use \myApp2\Movimiento\WhereReversoEntrada;
use \myApp2\Movimiento\WhereReversoSalida;

/** Movimiento: Clase para actualizar y consultar la tabla 'movimiento'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Movimiento
{

    /** Establece la ruta en que están ubicados los archivos .sql de la clase Movimiento.
    * Descripción: Establece la ruta en que están ubicados los .sql (querys) de la clase Movimiento.*/
    const PATH = __DIR__ . '/sql/movimiento/';

    /** Arreglo asociativo que contiene los correspondientes nombres de
    * campos y sus características respectivas. Dichos campos son los
    * únicos valores permitidos por la clase Movimiento a traves de la
    * variable $_POST, para actualizar la tabla 'movimiento'.*/
    static private $_aValReqs = [
        "id" => [
            "label" => "Id",
            "required" => false
        ],
        "id_movimiento_tipo" => [
            "label" => "Id Movimiento Tipo",
            "required" => false
        ],
        "movimiento_num" => [
            "label" => "Movimiento Num",
            "required" => false
        ],
        "movimiento_fec" => [
            "label" => "Movimiento Fec",
            "required" => false
        ],
        "sujeto" => [
            "label" => "Sujeto",
            "required" => false
        ],
        "descripcion" => [
            "label" => "Descripcion",
            "required" => false
        ],
        "id_soporte_tipo" => [
            "label" => "Id Soporte Tipo",
            "required" => false
        ],
        "soporte_num" => [
            "label" => "Soporte Num",
            "required" => false
        ],
        "soporte_fec" => [
            "label" => "Soporte Fec",
            "required" => false
        ],
        "str_arr_table" => [
            "label" => "strArrTable",
            "required" => false
        ],
        "observacion" => [
            "label" => "Observacion",
            "required" => false
        ],
        "cierre_fec" => [
            "label" => "Cierre Fec",
            "required" => false
        ],
        "id_user_insert" => [
            "label" => "Id User Insert",
            "required" => false
        ],
        "id_user_update" => [
            "label" => "Id User Update",
            "required" => false
        ],
        "id_user_edit" => [
            "label" => "Id User Edit",
            "required" => false
        ],
        "editing" => [
            "label" => "Editing",
            "required" => false
        ],
        "date_insert" => [
            "label" => "Date Insert",
            "required" => false
        ],
        "date_update" => [
            "label" => "Date Update",
            "required" => false
        ],
        "time_insert" => [
            "label" => "Time Insert",
            "required" => false
        ],
        "time_update" => [
            "label" => "Time Update",
            "required" => false
        ]
    ];

    /** Obtener registro(s) de la tabla 'movimiento'.
    * Descripción: Obtener registro(s) de la tabla 'movimiento'.
    * Si el parámetro $label trae el argumento 'LIST', devuelve todos los registros de la tabla 'movimiento'.
    * De lo contrario, si el parámetro $label trae el argumento 'REGIST', devuelve un solo registro,
    * siempre y cuando el valor de $_POST['id'] coincida con el ID principal de algún registro asociado.
    * Nota: Requiere el correspondiente valor $_POST['id'] del registro específico a consultar.
    * @param string $label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').
    * @return result Resultado con registro(s) de la tabla 'movimiento'.*/
    public function get($label, $method = 'all' )
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'ENTRADA':
                $arr['where'] = ' WHERE ' . WhereEntrada::$method();
                break;
            case 'SALIDA':
                $arr['where'] = ' WHERE ' . WhereSalida::$method();
                break;
            case 'REVERSO_ENTRADA':
                $arr['where'] = ' WHERE ' . WhereReversoEntrada::$method();
                break;
            case 'REVERSO_SALIDA':
                $arr['where'] = ' WHERE ' . WhereReversoSalida::$method();
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE A.id = {fld:id} ');
                break;
        }

        return DbFunc::exeQryFile(self::PATH . "get.sql", $arr, false, '', 'almacen');
    }

    /** Actualiza registro de la tabla 'movimiento'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'movimiento'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function register()
    {
        unset($_POST['id_soporte_num']);
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $_POST['id_usuario'] = $_SESSION['id_usuario'];
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "register.sql",$_POST, true, 'ACTUALIZANDO REGISTRO', 'almacen'));
            $msj = $row[0];
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
        }
        return $msj;
    }

    public function reverse()
    {
        unset($_POST['id_soporte_num']);
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $_POST['id_usuario'] = $_SESSION['id_usuario'];
            $_POST['str_arr_table'] = implode(", ", json_decode($_POST['str_arr_table']));
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "reverse.sql",$_POST, true, 'ACTUALIZANDO REGISTRO', 'almacen'));
            $msj = $row[0];
            $np = 2;
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
            $np = 1;
        }
        Func::adminMsj($msj,$np);
    }

    /** Elimina registro de la tabla 'movimiento'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'movimiento'.
    * Nota: Requiere el valor $_POST['id'] para buscar y eliminar registro en base de datos.*/ 
    public function remove()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::PATH . "remove.sql", $_POST, true, 'ELIMINANDO REGISTRO', 'almacen'));
        if ($row[0]!='B')
            Func::adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    public function loadRegist()
    {
        return array_key_exists('id', $_POST) ?
            DbFunc::fetchAssoc(self::get('REGIST')) :
                DbFunc::iniRegist('movimiento','public', 'almacen');
    }

    /** Lista de tabla foránea 'movimiento_tipo'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'movimiento_tipo'.
    * @return result Resultado de la consulta de la de tabla foránea 'movimiento_tipo'.*/ 
    public function movimientoTipoList()
    {
        return DbFunc::exeQryFile(self::PATH . 'movimiento_tipo_list_select.sql', $_POST, false, '', 'almacen');
    }

    /** Lista de tabla foránea 'soporte_tipo'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'soporte_tipo'.
    * @return result Resultado de la consulta de la de tabla foránea 'soporte_tipo'.*/ 
    public function soporteTipoList()
    {
        return DbFunc::exeQryFile(self::PATH . 'soporte_tipo_list_select.sql', $_POST, false, '', 'almacen');
    }

    public function movTypeConfig($label)
    {
        $arr = [];

        switch ($label) {
            case 'entrada':
                $arr = [
                    "id" => 1,
                    "label" => "Entrada"
                ];
                break;
            case 'salida':
                $arr = [
                    "id" => 2,
                     "label" => "Salida"
                ];
                break; 
            case 'reverso_entrada':
                $arr = [
                    "id" => 3,
                    "label" => "Reverso de Entrada"
                ];
                break;
            case 'reverso_salida':
                $arr = [
                    "id" => 4, 
                    "label" => "Reverso de Salida"
                ];
                break;
        }

        return $arr;

    }

}

