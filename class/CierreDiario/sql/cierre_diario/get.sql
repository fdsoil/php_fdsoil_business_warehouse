SELECT 
       A.cierre_dia AS cierre,
       A.id, 
       A.id_presentacion,
       A.cantidad_entrante AS entradas,
       A.cantidad_saliente AS salidas,
       A.cantidad_reverso_entrante AS reverso_entradas, 
       A.cantidad_reverso_saliente AS reverso_salidas,
       B.categoria,
       B.producto,
       B.marca,
       (SELECT public.view_presentacion_despliegue(A.id_presentacion)) AS empaque_des,
       B.empaque_json,
       B.medida_unidad,
       (SELECT view_total_antes_cierre(A.id_presentacion, A.cierre_dia)) AS acumulado,
       (SELECT view_total_antes_cierre(A.id_presentacion, A.cierre_dia)) 
       + (COALESCE(a.cantidad_entrante, 0) 
           - COALESCE(a.cantidad_reverso_entrante, 0) 
           - (COALESCE(a.cantidad_saliente, 0) 
                   - COALESCE(a.cantidad_reverso_saliente, 0))) AS total
  FROM public.cierre_diario A
  INNER JOIN public.view_presentacion B ON A.id_presentacion = B.id
  {fld:where}
  ORDER BY 1;



