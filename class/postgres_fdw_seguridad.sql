CREATE SERVER localhost_negocio_seguridad FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', dbname 'negocio_seguridad', port '5432');

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_seguridad OPTIONS(user 'postgres', password 'postgres');

CREATE FOREIGN TABLE seguridad_usuario (id integer, usuario character varying, correo character varying,
cedula character varying, nombre character varying, apellido character varying ) 
SERVER localhost_negocio_seguridad OPTIONS(schema_name 'seguridad', table_name 'usuario');

SELECT id, usuario, correo, cedula, nombre, apellido
  FROM seguridad_usuario;
--/////////////////////////////////////////////


CREATE SERVER localhost_negocio_producto FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', dbname 'negocio_producto', port '5432');

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_producto OPTIONS(user 'postgres', password 'postgres');

CREATE FOREIGN TABLE view_categoria_recursivo (id integer, nombre character varying, familia text,
padre_id integer, nivel integer, final boolean ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_categoria_recursivo');

select * from view_categoria_recursivo;




--/////////////////////////////////////

CREATE FOREIGN TABLE view_categoria (
	id integer,
	nombre character varying,
	familia text,
	padre_id integer,
	nivel integer,
	final boolean ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_categoria');
SELECT * FROM view_categoria;

CREATE FOREIGN TABLE view_producto (
	id integer,
	categoria text,
	producto character varying,
	marca character varying ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_producto');
SELECT * FROM view_producto;

CREATE FOREIGN TABLE view_presentacion (
       id integer, 
       categoria text,
       producto character varying,
       marca character varying,
       empaque_json jsonb,
       medida_unidad character varying, 
       precio_costo double precision,
       precio_venta double precision, 
       bar_cod character varying, 
       int_cod character varying
) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_presentacion');
SELECT * FROM view_presentacion;

SELECT A.id, (SELECT view_presentacion_despliegue(A.id)) from view_presentacion A





-------/*
-- View: public.view_presentacion

-- DROP VIEW public.view_presentacion;

--CREATE OR REPLACE VIEW public.view_presentacion AS 
-- SELECT a.id,
--    c.familia AS categoria,
--    b.nombre AS producto,
--    d.nombre AS marca,
--    a.nombre AS presentacion,
--    ( SELECT presentacion_despliegue(a.id)::character varying AS presentacion_despliegue) AS presentacion_despliegue,
--    a.empaque AS empaque_json,
--    a.medida_unidad,
--    a.precio_costo,
--    a.precio_venta,
--    a.bar_cod,
--    a.int_cod
--   FROM presentacion a
--     JOIN producto b ON a.id_producto = b.id
--     JOIN view_categoria c ON b.id_categoria = c.id
--     JOIN marca d ON b.id_marca = d.id
--  ORDER BY c.familia, b.nombre, d.nombre, a.nombre;

-- ALTER TABLE public.view_presentacion
--  OWNER TO postgres;
--*/



drop cascades to foreign table view_categoria
drop cascades to foreign table view_presentacion
drop cascades to view view_existencia_cierre_diario
drop cascades to view view_existencia_movimiento
drop cascades to view view_existencia
drop cascades to foreign table view_producto
drop cascades to foreign table view_categoria_recursivo



drop USER MAPPING FOR admin SERVER localhost_negocio_producto;
drop foreign table view_categoria;
drop SERVER localhost_negocio_producto CASCADE;

CREATE SERVER localhost_negocio_producto FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '169.10.1.3', dbname 'ejjc_negocio_producto', port '5432');

CREATE USER MAPPING FOR admin SERVER localhost_negocio_producto OPTIONS(user 'admin', password 'admin');

CREATE FOREIGN TABLE view_categoria (id integer, nombre character varying, familia text,
padre_id integer, nivel integer, final boolean ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_categoria');

select * from view_categoria;

--///////////////////////////////////////////////////////////////
--///////////////////////////////////////////////////////////////
--//////////////////////////////////////////////////////////////

-- Extension: dblink

-- DROP EXTENSION dblink;

 CREATE EXTENSION dblink
  SCHEMA public
  VERSION "1.2";
-- Extension: postgres_fdw

-- DROP EXTENSION postgres_fdw;

 CREATE EXTENSION postgres_fdw
  SCHEMA public
  VERSION "1.0";


drop USER MAPPING FOR admin SERVER localhost_negocio_producto;
drop foreign table view_categoria;
drop SERVER localhost_negocio_producto CASCADE;

CREATE SERVER localhost_negocio_producto FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '169.10.1.3', dbname 'ejjc_negocio_producto', port '5432');

CREATE USER MAPPING FOR admin SERVER localhost_negocio_producto OPTIONS(user 'admin', password 'admin');

CREATE FOREIGN TABLE view_categoria (id integer, nombre character varying, familia text,
padre_id integer, nivel integer, final boolean ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_categoria');

select * from view_categoria;


CREATE FOREIGN TABLE view_producto (
	id integer,
	categoria text,
	producto character varying,
	marca character varying ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_producto');
SELECT * FROM view_producto;


CREATE FOREIGN TABLE view_presentacion (
       id integer, 
       categoria text,
       producto character varying,
       marca character varying,
       empaque_json jsonb,
       medida_unidad character varying, 
       precio_costo double precision,
       precio_venta double precision, 
       bar_cod character varying, 
       int_cod character varying
) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_presentacion');
SELECT * FROM view_presentacion;


/*
Lo siento Ya tenemos demasiados clientes
CREATE FOREIGN TABLE view_producto (
	id integer,
	categoria text,
	producto character varying,
	marca character varying ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_producto');
SELECT * FROM view_producto;


CREATE FOREIGN TABLE view_presentacion (
       id integer, 
       categoria text,
       producto character varying,
       marca character varying,
       empaque_json jsonb,
       medida_unidad character varying, 
       precio_costo double precision,
       precio_venta double precision, 
       bar_cod character varying, 
       int_cod character varying
) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_presentacion');
SELECT * FROM view_presentacion;

drop USER MAPPING FOR postgres SERVER localhost_negocio_producto;
drop foreign table view_categoria;
drop SERVER localhost_negocio_producto CASCADE;

CREATE SERVER localhost_negocio_producto FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.1', dbname 'negocio_tienda', port '5432');

CREATE USER MAPPING FOR postgres SERVER localhost_negocio_producto OPTIONS(user 'postgres', password 'postgres');

CREATE FOREIGN TABLE view_categoria (id integer, nombre character varying, familia text,
padre_id integer, nivel integer, final boolean ) 
SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_categoria');

select * from view_categoria;
*/




--drop USER MAPPING FOR postgres SERVER localhost_negocio_producto;
--drop SERVER localhost_negocio_producto CASCADE;

--CREATE SERVER localhost_negocio_producto 
--	FOREIGN DATA WRAPPER postgres_fdw 
--		OPTIONS (host '127.0.0.1', dbname 'negocio_producto', port '5432');

--CREATE USER MAPPING FOR postgres 
--	SERVER localhost_negocio_producto
--		OPTIONS(user 'postgres', password 'postgres');

--CREATE FOREIGN TABLE view_categoria (id integer, nombre character varying, familia text,
--padre_id integer, nivel integer, final boolean ) 
--SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_categoria');

--select * from view_categoria;

--CREATE FOREIGN TABLE view_producto (
--	id integer,
--	categoria text,
--	producto character varying,
--	marca character varying ) 
--SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_producto');
--SELECT * FROM view_producto;


--CREATE FOREIGN TABLE view_presentacion (
--       id integer, 
--       categoria text,
--       producto character varying,
--       marca character varying,
--       empaque_json jsonb,
--       medida_unidad character varying, 
--       precio_costo double precision,
--       precio_venta double precision, 
--       bar_cod character varying, 
--       int_cod character varying
--) 
--SERVER localhost_negocio_producto OPTIONS(schema_name 'public', table_name 'view_presentacion');
--SELECT * FROM view_presentacion;









