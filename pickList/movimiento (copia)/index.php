<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        //\FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $result = \myApp2\Movimiento::get('ENTRADA');
        while ($row = \FDSoil\DbFunc::fetchAssoc($result)) {
            $xtpl->assign('DESCRIPCION', $row['movimiento_num']);
            $xtpl->assign('DESCRIPCION2', Func::change_date_format($row['movimiento_fec']));
            /*$xtpl->assign('MOVIMIENTO_NUM', $row['movimiento_num']);
            $xtpl->assign('MOVIMIENTO_FEC', Func::change_date_format($row['movimiento_fec']));
            $xtpl->assign('SUJETO', $row['sujeto']);
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            //$xtpl->assign('ID_SOPORTE_TIPO', $row['id_soporte_tipo']);
            $xtpl->assign('SOPORTE_NUM', $row['soporte_num']);
            //$xtpl->assign('SOPORTE_FEC', Func::change_date_format($row['soporte_fec']));
            //$xtpl->assign('OBSERVACION', $row['observacion']);
            //$xtpl->assign('CIERRE_FEC', Func::change_date_format($row['cierre_fec']));            
            $xtpl->assign('DES_MOVIMIENTO_TIPO', $row['des_movimiento_tipo']);
            $xtpl->assign('DES_SOPORTE_TIPO', $row['des_soporte_tipo']);*/
            $xtpl->parse('main.nivel_0');
        }
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

