<?php
use \FDSoil\Func;

class SubIndex
{
    public function execute($arr)
    {
        \FDSoil\Audit::validaReferenc();

        clearstatcache();


        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $result = \myApp2\Movimiento::get($arr[0], 'notReversed');
        $xtpl->assign('MOVIMIENTO_TIPO', ucfirst(strtolower($arr[0])));
        while ($row = \FDSoil\DbFunc::fetchAssoc($result)) {
            $xtpl->assign('ID', $row['id']);
            $xtpl->assign('MOVIMIENTO_NUM', $row['movimiento_num']);
            $xtpl->assign('MOVIMIENTO_FEC', Func::change_date_format($row['movimiento_fec']));
            $xtpl->assign('DES_SOPORTE_TIPO', $row['des_soporte_tipo']);
            $xtpl->assign('SOPORTE_NUM', $row['soporte_num']);           
            $xtpl->parse('main.nivel_0');
        }
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

