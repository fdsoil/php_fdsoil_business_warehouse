<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        //\FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $result = \myApp1\Categoria::get('WITH_ASSOC_PRESENTATION');;
        while ($row = \FDSoil\DbFunc::fetchAssoc($result)) {
            $xtpl->assign('NIVEL_0_ID', $row["id"]);
            $xtpl->assign('NIVEL_O_DES', $row["familia"]);
            $xtpl->parse('main.nivel_0');
        }
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

