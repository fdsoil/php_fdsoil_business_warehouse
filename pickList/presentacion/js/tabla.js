var tmpNivel=new Array();
tmpNivel[1]='';
tmpNivel[2]='';

function numIndex(numNivel){
    var numIndex=0;
    if (numNivel==2)
	numIndex=2;
    else if (numNivel==3)
	numIndex=4;
    else if (numNivel==4)
	numIndex=6;
    else if (numNivel==5)
	numIndex=8;
    return numIndex;
}

function buscarDatos(numNivel, id){
	if (numNivel==1)
		sendNivel1(id);
	else if (numNivel==2)
		sendNivel2(id);
	else if (numNivel==3)
		sendNivel3(id);
	else if (numNivel==4)
		sendNivel4(id);
	else if (numNivel==5)
		sendNivel5(id);
}

function llenarNivel(numNivel, objNivel){
    if (tmpNivel[numNivel]!=objNivel.id){
	borrarNivel(numNivel);
        tmpNivel[numNivel]=objNivel.id;	
	var arreglo=tmpNivel[numNivel].split('_');
        buscarDatos(numNivel, arreglo[numIndex(numNivel)]);
     }  
}

function borrarNivel(numNivel){
	var objPadre=document.getElementById(tmpNivel[numNivel]);
	var objHijo=document.getElementById(tmpNivel[numNivel]+'_t'+numNivel);
	if (tmpNivel[numNivel]!='')
 		objPadre.removeChild(objHijo);
	for (i=tmpNivel.length;i>numNivel;i--)
	    tmpNivel[i]='';
}

function createNivel(objs,idNivel, onClickFunction, strTblNum, classDiv, numDes, classTable, guion){
        //alert(objs);
	var tabla=document.createElement("table");
	var tbody=document.createElement("tbody");
	var objsTr = new Array(); 
	tabla.id=idNivel+strTblNum;
	for (i=0; i<(objs.length); i++){
        	objsTr[i]=addNivel(objs[i], tabla.id, onClickFunction, classDiv, numDes, classTable, guion, strTblNum);
        	tbody.appendChild(objsTr[i]);
    	}	
   	tabla.appendChild(tbody);
   	document.getElementById(idNivel).appendChild(tabla);
}

function addNivel(sub_arreglo, idTabla, onClickFunction, classDiv, numDes, classTable, guion, strTblNum){
    var tr  = document.createElement('tr');
    var td  = document.createElement('td');
    var div  = document.createElement('div'); 
    div.setAttribute('onclick',onClickFunction); 
    div.setAttribute('class',classDiv); 
    div.id=idTabla+'_'+sub_arreglo['id'];
    var tbl = document.createElement('table');
    tbl.setAttribute('class', classTable);    
    var row  = document.createElement('tr');
    var cell1  = document.createElement('td');
    let field_name = '';
    let simbol1 = '';
    let simbol2 = '';
    if (strTblNum == '_t1') {
        field_name = 'des_marca';
        simbol1 = ' ( ';
        simbol2 = ' )';
    } else if (strTblNum == '_t2') {
        simbol1 = '| ';
        field_name = 'empaque_desplegado';
    }
    var node1  = document.createTextNode(guion+ simbol1 + sub_arreglo[field_name] + simbol2 );
    cell1.appendChild(node1);
    cell1.setAttribute('width', numDes+'px');
    cell1.setAttribute('align', 'left');
    row.appendChild(cell1);
    tbl.appendChild(row);
    tbl.setAttribute('border',0); 
    div.appendChild(tbl);
    td.appendChild(div);
    tr.appendChild(td);        
    return tr;
}


