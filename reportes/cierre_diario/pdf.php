<?php 
use \FDSoil\Func;
use \FDSoil\DbFunc;
use \FDSoil\TCPDF;
use \myApp2\CierreDiario;


class MYTCPDF extends TCPDF
{

    public function DrawHeader($header, $w)
    {
        $this->SetY(30); $this->SetX(95); $this->writeHTML("Cierre(s) Diario(s)");
        $this->SetFillColor(85,139,47);     
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(0.3);
        //$this->SetFont('', 'B', 8);
        //$pdf->SetFont ("helvetica", "", $font_size , "", "default", true );      
        $num_headers = count($header);        
        $this->MultiCell(15, 10, 'Fecha de Cierre', 0, 'C', 1, 0,'', '', true, 0,false,true,10,'M');
        $this->MultiCell(35, 10, 'Producto', 0, 'C', 1, 0,'', '', true, 0,false,true,10,'M');
        $this->MultiCell(54, 10, 'Presentación', 0, 'C', 1, 0, '', '', true, 0,false,true,10,'M');
        $this->MultiCell(16, 10, 'Acumulado', 0, 'C', 1, 0, '', '', true, 0,false,true,10,'M');
        $this->MultiCell(15, 10, 'Entrada', 0, 'C', 1, 0, '', '', true, 0,false,true,10,'M');
        $this->MultiCell(15, 10, 'Salida', 0, 'C', 1, 0, '', '', true, 0,false,true,10,'M');
        $this->MultiCell(15, 10, 'Reverso de Entradas', 0, 'C', 1, 0, '', '', true, 0,false,true,10,'M');
        $this->MultiCell(15, 10, 'Reverso de Salidas', 0, 'C', 1, 0, '', '', true, 0,false,true,10,'M');
        $this->MultiCell(15, 10, 'Existencia al Cierre', 0, 'C', 1, 0,'', '', true, 0,false,true,10,'M');     
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
    }

    function ColoredTable($header,$data) 
    {
        $w = array(15, 35, 54, 16, 15, 15, 15, 15, 15);
        $this->DrawHeader($header, $w);
        // Data
        $fill = 0;
        foreach($data as $row) {
            //Get current number of pages.
            $num_pages = $this->getNumPages();
            $this->startTransaction();
            $this->MultiCell($w[0], 15, Func::change_date_format($row['cierre']), 0, 'C', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[1], 15, $row['categoria'].'/'.$row['producto'].'('.$row['marca'].')', 0, 'L', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[2], 15, $row['empaque_des'], 0, 'L', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[3], 15, $row['acumulado'], 0, 'R', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[4], 15, $row['entradas'], 0, 'R', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[5], 15, $row['salidas'], 0, 'R', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[6], 15, $row['reverso_entradas'], 0, 'R', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[7], 15, $row['reverso_salidas'], 0, 'R', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->MultiCell($w[8], 15, $row['total'], 0, 'R', $fill, 0, '', '', true, 0,false,true,15,'M');
            $this->Ln();
            //If old number of pages is less than the new number of pages,
            //we hit an automatic page break, and need to rollback.
            if($num_pages < $this->getNumPages()) {
                //Undo adding the row.
                $this->rollbackTransaction(true);
                //Adds a bottom line onto the current page. 
                //Note: May cause page break itself.
                $this->Cell(array_sum($w), 0, '', 'T');
                //Add a new page.
                $this->AddPage();
                //Draw the header.
                $this->DrawHeader($header, $w);
                //Re-do the row.
               /* $this->Cell($w[0], 6, $row[0], 'LR', 0, 'C', $fill);
                $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
                $this->Cell($w[2], 6, $row[2], 'LR', 0, 'C', $fill);
                $this->Cell($w[3], 6, $row[3], 'LR', 0, 'C', $fill);
                $this->Cell($w[4], 6, $row[4], 'LR', 0, 'C', $fill);
                $this->Cell($w[5], 6, $row[5], 'LR', 0, 'C', $fill);
                $this->Cell($w[6], 6, $row[6], 'LR', 0, 'C', $fill);*/
                //$this->Ln();
                // $this->Footer();
            } else {
                //Otherwise we are fine with this row, discard undo history.
                $this->commitTransaction();
            }
            $fill=!$fill;

        }
        $this->Cell(array_sum($w), 0, '', 'T');

    }

    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('Helvetica', '', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

class SubIndex
{ 

    public function execute()
    {
        $data = DbFunc::fetchAll(\myApp2\CierreDiario::get('FILTER'));
        //$data = array_merge($data,$data);
        $datacount = count($data);
        $i=0;       
        $pdf = new MYTCPDF( PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetFont('', 'B', 7);
        while ($i < $datacount) {
            $dataout = array_slice($data, $i, 35, false);
            $pdf->AddPage();
            // print colored table      
            $pdf->ColoredTable($data[0], $dataout);
            $i = $i + 54;
        }
        $date = new DateTime();
        $result = $date->format('Y-m-d H:i:s');  
        return $pdf->Output("CierreDiario".$date->format('Y-m-d H:i:s').".pdf", "I");
    }
    
}
SubIndex::execute();

