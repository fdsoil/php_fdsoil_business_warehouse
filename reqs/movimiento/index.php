<?php

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function register()
    {
        echo base64_encode(\myApp2\Movimiento::register());
    }

    private function get() 
    {
        echo base64_encode(
            json_encode(
                \FDSoil\DbFunc::fetchAll(
                    \myApp2\Movimiento::get('REGIST')
                )
            )
        );
    }

}


