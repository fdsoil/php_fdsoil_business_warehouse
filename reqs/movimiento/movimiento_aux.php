<?php
use \myApp2\Movimiento\MovimientoAux;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function register() { echo base64_encode(MovimientoAux::register()); }

    private function get() { echo base64_encode(json_encode(MovimientoAux::get())); }

    private function remove() { echo base64_encode(MovimientoAux::remove()); }

}

